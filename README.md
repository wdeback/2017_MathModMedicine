Introduction
------------

Slides for the mathematical modeling of tissues in medicine. 

Focus on cardiac modeling and intestinal crypts. 

Prepared for the 2017 Winter semester course at IMB, Faculty of Medicine, TU Dresden, Germany.


Author
-------

Walter de Back, PhD

Institute for Medical Informatics and Biometry (IMB)

Faculty of Medicine Carl Gustav Carus

TU Dresden

Germany

web: http://walter.deback.net

twitter: @wdeback

Contributions
-------------

The slides include some material from Gary Miriams (Nottingham, UK) cardiac simulations and James Osborne (Melbourne, AU) on intestinal crypt 
simulations.
